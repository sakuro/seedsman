module Seedsman
  class Result < Struct.new(:created, :changed, :unchanged, :failed)
    def initialize
      members.each {|member| self[member] = [] }
    end

    def summary
      result = []
      result << each_pair.map {|member, ids| '%s: %d' % [ member.capitalize, ids.size ] }.to_sentence
      unless success?
        result << "Failures:"
        self[:failed].each do |(id, message)|
          result << '- %d: %s' % [id, message]
        end
      end
      result.join("\n")
    end

    def success?
      self[:failed].empty?
    end
  end
end

