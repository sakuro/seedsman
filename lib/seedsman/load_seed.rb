require 'seedsman/dependencies'
require 'seedsman/active_record/seed_loadable'

module Seedsman
  module LoadSeed
    extend ActiveSupport::Concern

    included do
      ::ActiveRecord::Tasks::DatabaseTasks.seed_loader = Seedsman
    end

    module ClassMethods
      using ::Seedsman::ActiveRecord::SeedLoadable

      def load_seed
        dependencies = Seedsman::Dependencies.load
        dependencies.each do |(dependee, _)|
          dependee.classify.constantize.load_seed
        end
      end
    end
  end
end

Seedsman.include(Seedsman::LoadSeed)
