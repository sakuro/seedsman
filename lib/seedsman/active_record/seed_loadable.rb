require 'ruby-progressbar'
require 'seedsman/result'

module Seedsman
  module ActiveRecord
    module SeedLoadable
      refine ::ActiveRecord::Base.singleton_class do
        def load_seed
          seed_entries = seed_files.map {|seed_file| YAML.load_file(seed_file)}.inject(:+)
          result = Seedsman::Result.new
          progress = ProgressBar.create(title: name, format: '%t|%B%c/%C %P%%', total: seed_entries.size)

          transaction do
            seed_entries.each do |attributes|
              record = find_or_initialize_by(id: attributes['id'])
              record.assign_attributes(attributes)
              state = update_state(record)
              begin
                record.save!
                result[state] << record.id
              rescue ::ActiveRecord::RecordInvalid => e
                result[:failed] << [record.id, e.message]
              end
              progress.increment
            end
          end

          progress.log(result.summary)

          # Delete records not listed in the seed files
          transaction do
            phantom_ids = pluck(:id) - seed_entries.map {|attributes| attributes['id']}
            if phantom_ids.present?
              delete(phantom_ids)
              progress.log 'Removed phantom IDs: %p' % phantom_ids
            end
          end

          result
        end

        def seed_files
          seed_file_patterns.flat_map {|pattern| Pathname.glob(pattern) }.sort_by(&:to_s)
        end

        def seed_file_patterns
          seed_dirs = Rails.application.config.paths['db/seeds'].existent_directories
          base = name.underscore.pluralize
          seed_dirs.map {|dir| File.join(dir, "{#{base}.yml,#{base}/*.yml}") }
        end

        def update_state(record)
          case
          when record.new_record? ; :created
          when record.changed?    ; :changed
          else                    ; :unchanged
          end
        end
      end
    end
  end
end
