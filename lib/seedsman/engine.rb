module Seedsman
  class Engine < Rails::Engine
    initializer :add_seeds_path do |app|
      app.paths.add 'db/seeds'
      app.paths.add 'db/seeds/dependencies.yml'
    end
  end
end
