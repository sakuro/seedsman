require 'tsort'
require 'yaml'

module Seedsman
  class Dependencies
    include Enumerable

    class Error < ::StandardError; end
    class LoadError < Error; end

    def initialize(params)
      raise ArgumentError unless Hash === params
      raise ArgumentError if params.empty?
      dependencies = DependenciesHash.new
      params.each {|dependent, dependees| dependencies[dependent] = dependees }
      @sorted ||= dependencies.tsort.map {|dependent| [dependent, dependencies[dependent] ] }
    rescue ArgumentError, TSort::Cyclic
      raise Error
    end

    def [](dependee)
      _, dependents = find {|(a, _)| a == dependee }
      raise ArgumentError unless dependents
      dependents
    rescue ArgumentError
      raise Error
    end

    delegate :each, to: :@sorted

    class << self
      def load
        dependencies_path = Rails.application.config.paths['db/seeds/dependencies.yml'].existent.first
        raise LoadError, 'dependencies.yml could not be found' if dependencies_path.nil?
        raw_dependencies = YAML.load_file(dependencies_path)
        new(raw_dependencies)
      rescue Errno::ENOENT, Psych::SyntaxError, Error
        raise LoadError
      end
    end

    private

    class DependenciesHash < Hash
      include TSort
      alias tsort_each_node each_key
      def tsort_each_child(node, &block)
        fetch(node).each(&block)
      end
      def []=(key, value)
        values = [ value ].flatten
        super(key, values)
        values.each do |dependee|
          super(dependee, []) unless key?(dependee)
        end
      end
    end
    private_constant :DependenciesHash
  end
end
