require 'rake/clean'

CLEAN.include('coverage')
CLEAN.include('pkg')
CLEAN.include('doc')
CLEAN.include('spec/*/log/*.log')

CLOBBER.include('vendor')
CLOBBER.include('spec/*/db/*.sqlite3')
