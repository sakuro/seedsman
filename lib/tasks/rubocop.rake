begin
  require 'rubocop/rake_task'

  RuboCop::RakeTask.new do |t|
    t.formatters = %w(simple)
    t.options += %w(--rails)
  end
rescue LoadError
  warn 'RuboCop is not available'
end
