# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'seedsman/version'

Gem::Specification.new do |spec|
  spec.name          = "seedsman"
  spec.version       = Seedsman::VERSION
  spec.authors       = ["OZAWA Sakuro"]
  spec.email         = ["sakuro@2238club.org"]
  spec.summary       = %q{rake db:seed using db/seeds/*.yml}
  spec.description   = %q{Seedsman helps the task of managing seed data by putting them in YAML files like fixtures.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]


  spec.add_dependency 'ruby-progressbar'
  spec.add_dependency 'rails', '~> 4.2.0'
  spec.add_development_dependency 'rspec-rails', '~> 3.2.0'
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency 'yard'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov-console'
end
