require 'spec_helper'

RSpec.describe Seedsman::ActiveRecord::SeedLoadable do
  describe '.load_seed' do

    using ::Seedsman::ActiveRecord::SeedLoadable

    let(:progress_bar) { double(:progress_bar).as_null_object }
    before { allow(ProgressBar).to receive(:create).and_return(progress_bar) }

    it 'loads seeds from corresponding YAML file' do
      expect { Region.load_seed }.to change(Region, :count).from(0).to(2)
    end
    it 'loads seeds from all YAML files in corresponding directory' do
      expect { City.load_seed }.to change(City, :count).from(0).to(110)
    end
  end
end
