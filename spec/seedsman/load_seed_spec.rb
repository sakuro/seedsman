require 'spec_helper'

RSpec.describe Seedsman do
  describe '.load_seed' do
    let(:dependencies) { ::Seedsman::Dependencies.new('cities' => 'prefectures', 'prefectures' => 'regions') }

    before do
      allow(Seedsman::Dependencies).to receive(:load).and_return(dependencies)
    end

    # TODO: does not work; needs investigation
    using ::Seedsman::ActiveRecord::SeedLoadable
    # workaround
    class << ::ActiveRecord::Base
      def load_seed; end
    end

    it 'calls .load_seed of models in order' do
      expect(Region).to receive(:load_seed).ordered
      expect(Prefecture).to receive(:load_seed).ordered
      expect(City).to receive(:load_seed).ordered

      Seedsman.load_seed
    end
  end
end

RSpec.describe ActiveRecord::Tasks::DatabaseTasks do
  describe '.seed_loader' do
    it 'is Seedsman' do
      expect(ActiveRecord::Tasks::DatabaseTasks.seed_loader).to eq(Seedsman)
    end
  end
end
