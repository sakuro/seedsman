require 'spec_helper'
require 'tempfile'

RSpec.describe Seedsman::Dependencies do

  describe '.load' do
    before do
      if dependencies_content
        @temp_deps = Tempfile.open(['dependencies.', '.yml'])
        @temp_deps.print dependencies_content
        @temp_deps.close(false)

        Rails.application.config.paths['db/seeds/dependencies.yml'] = @temp_deps.path
      end
    end
    after do
      @temp_deps.close! if @temp_deps
      Rails.application.config.paths['db/seeds/dependencies.yml'] = 'db/seeds/dependencies.yml'
    end

    let(:dependencies_content) { nil }

    context 'when dependencies file is valid' do
      let(:dependencies_content) {
        <<-EOF.strip_heredoc
          cities: prefectures
          prefectures: regions
        EOF
      }
      it 'returns an instance of Seedsman::Dependencies' do
        expect(Seedsman::Dependencies.load).to be_an_instance_of(Seedsman::Dependencies)
      end
    end

    context 'when dependencies file is broken' do
      let(:dependencies_content) { 'hello, world: [' }
      it 'raises LoadError' do
        expect { Seedsman::Dependencies.load }.to raise_error(Seedsman::Dependencies::LoadError)
      end
    end

    context 'when dependencies file does not contain a Hash' do
      let(:dependencies_content) { '[]' }
      it 'raises LoadError' do
        expect { Seedsman::Dependencies.load }.to raise_error(Seedsman::Dependencies::LoadError)
      end
    end

    context 'when dependencies file does not exist' do
      before do
        Rails.application.config.paths['db/seeds/dependencies.yml'] = 'not_exist.yml'
      end
      it 'raises LoadError' do
        expect(Rails.application.config.paths['db/seeds/dependencies.yml'].existent).to be_empty
        expect { Seedsman::Dependencies.load }.to raise_error(Seedsman::Dependencies::LoadError)
      end
    end
  end

  describe '#initialize' do
    let(:new_dependencies) { Seedsman::Dependencies.new(params) }

    context 'with cyclic dependencies' do
      let(:params) { {'cities' => 'prefectures', 'prefectures' => 'cities'} }
      it 'raises Error' do
        expect { new_dependencies }.to raise_error(Seedsman::Dependencies::Error)
      end
    end

    context 'with no dependencies' do
      let(:params) { {} }
      it 'raises Error' do
        expect { new_dependencies }.to raise_error(Seedsman::Dependencies::Error)
      end
    end
  end

  describe '#[]' do
    let(:dependencies) { Seedsman::Dependencies.new('cities' => 'prefectures') }

    describe 'given dependee with dependents' do
      subject { dependencies['cities'] }
      it { is_expected.to eq(%w(prefectures)) }
    end

    describe 'given dependee without dependents(only appears on dependents)' do
      subject { dependencies['prefectures'] }
      it { is_expected.to eq([]) }
    end

    describe 'given not-dependee' do
      it 'raises Error' do
        expect { dependencies['stations'] }.to raise_error(Seedsman::Dependencies::Error)
      end
    end
  end

  describe '#each' do
    let(:dependencies) { Seedsman::Dependencies.new('cities' => 'prefectures', 'prefectures' => 'regions') }
    it 'yields (dependee, dependents) pair from less dependent one first' do
      expect {|b|
        dependencies.each(&b)
      }.to yield_successive_args(
        ['regions', []],
        ['prefectures', ['regions']],
        ['cities', ['prefectures']]
      )
    end
  end
end
