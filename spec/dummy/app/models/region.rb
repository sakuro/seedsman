class Region < ActiveRecord::Base
  has_many :prefectures

  validates :name, presence: true, uniqueness: true
end
