class Prefecture < ActiveRecord::Base
  belongs_to :region

  validates :name, presence: true, uniqueness: true
  validates :region_id, presence: true
end
