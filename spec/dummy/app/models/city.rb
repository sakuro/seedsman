class City < ActiveRecord::Base
  belongs_to :prefecture

  validates :name, presence: true
  validates :prefecture_id, presence: true
end
