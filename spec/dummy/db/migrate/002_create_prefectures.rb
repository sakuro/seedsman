class CreatePrefectures < ActiveRecord::Migration
  def change
    create_table :prefectures do |t|
      t.string :name
      t.references :region, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :prefectures, :regions, on_delete: :restrict
  end
end
